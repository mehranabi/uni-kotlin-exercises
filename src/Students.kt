class Student(var name: String, var age: Int) {
    fun isAbove19(): Boolean {
        return age > 19
    }
}

fun main() {
    val students: Array<Student> = arrayOf(
            Student("Mehran", 20),
            Student("Arman", 15),
            Student("Behzad", 22),
            Student("Maryam", 18),
            Student("Samira", 21),
            Student("Sara", 17)
    )

    println("Adult Students (Above 19 y.o.):")
    val adults: List<Student> = students.filter { student: Student -> student.isAbove19() }
    adults.forEach { student: Student -> println("${student.name} (${student.age})") }
}