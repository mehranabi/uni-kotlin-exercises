import kotlin.math.pow
import kotlin.math.sqrt

fun main() {
    println("Take (ax^2 + bx + c) as the template and provide the inputs.")

    print("Enter a: ")
    val a: Int = convert(readLine())

    print("Enter b: ")
    val b: Int = convert(readLine())

    print("Enter c: ")
    val c: Int = convert(readLine())

    val delta: Int = calcDelta(a, b, c)
    val root1: Double
    val root2: Double

    when {
        delta < 0 -> {
            println("(DETERMINANT = 0) Equation doesn't have real roots!")
        }
        delta == 0 -> {
            root1 = calcOneRoot(a, b)
            println("Root1 = Root2 = $root1")
        }
        else -> {
            val roots = calcRoots(a, b, delta)
            root1 = roots[0]
            root2 = roots[1]
            println("Root1 = $root1")
            println("Root2 = $root2")
        }
    }

}

fun calcRoots(a: Int, b: Int, delta: Int): DoubleArray {
    val deltaSqrt = sqrt(delta.toDouble())
    val minB = -b
    val twoA = 2 * a

    val root1 = (minB + deltaSqrt) / twoA
    val root2 = (minB - deltaSqrt) / twoA

    return doubleArrayOf(root1, root2)
}

fun calcOneRoot(a: Int, b: Int): Double {
    return ((-b).toDouble() / (2 * a))
}

fun calcDelta(a: Int, b: Int, c: Int): Int {
    return (b.toDouble().pow(2) - (4 * a * c)).toInt()
}

fun convert(s: String?): Int {
    if (s != null) {
        return s.toInt()
    }
    return 0
}