fun main() {
    var max = 10

    println("Welcome to guess game!")
    print("What's maximum number you wanna guess? ")

    val input = readLine()
    if (input != null) {
        max = input.toInt()
    }

    val random = (0..max).random()
    var run = true

    do {
        val guess = takeGuess()
        when {
            guess == random -> {
                println("Congrats! You won the game.")
                run = false
            }
            guess < random -> {
                println("Take your guess higher!")
            }
            else -> {
                println("Lower your guess!")
            }
        }
    } while(run)
}

fun takeGuess(): Int {
    print("Enter your guess: ")
    val guess = readLine()

    if (guess != null) {
        return guess.toInt()
    }

    return 0
}